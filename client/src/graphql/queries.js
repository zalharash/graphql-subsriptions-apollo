import gql from 'graphql-tag';
import client from './client';
//-------------
const messagesQuery = gql`
  query MessagesQuery {
    messages {
      id
      from
      text
    }
  }
`;
//-------------
const addMessageMutation = gql`
  mutation AddMessageMutation($input: MessageInput!) {
    message: addMessage(input: $input) {
      id
      from
      text
    }
  }
`;
// Subscription:
//-------------
const messageAddedSubscription = gql`
  subscription {
    messageAdded {
      id
      from
      text
    }
  }
`;

//-------------
export async function addMessage(text) {
  const { data } = await client.mutate({
    mutation: addMessageMutation,
    variables: { input: { text } }
  });
  return data.message;
}

//-------------
export async function getMessages() {
  const { data } = await client.query({ query: messagesQuery });
  return data.messages;
}

/**
 * We have two subscribe functions call here:
 * 1. @client.subscribe: We started the subscription by calling client.subscribe, this initiate the graphql
 * subscription with the server, which means the server will send messages to this client over Websocket
 * 2. @Observable.subscribe: to dispatch messages to different React components inside our application,
 * e.i say we have a Chat.js component to display the new dispatched msgs and another component to notify
 * some other components about the new messages, or to gave them a reference to unsbscribe ...
 * @Summary: the client.subscribe receives msgs from the server, and observable.subscribe dipatches
 * those messages to other components
 */
//-------------
export function onMessageAdded(handleMessage) {
  const observable = client.subscribe({ query: messageAddedSubscription });

  return observable.subscribe(({ data }) => {
    handleMessage(data.messageAdded);
  });
}
