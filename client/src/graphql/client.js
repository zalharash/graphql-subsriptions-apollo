import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  split
} from 'apollo-boost';
import { getAccessToken } from '../auth';
// Implement Subscription using apollo websocket
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

//---------------
const httpUrl = 'http://localhost:9000/graphql';
const wsUrl = 'ws://localhost:9000/graphql';

//---------------
const httpLink = ApolloLink.from([
  new ApolloLink((operation, forward) => {
    const token = getAccessToken();
    if (token) {
      operation.setContext({ headers: { authorization: `Bearer ${token}` } });
    }
    return forward(operation);
  }),
  new HttpLink({ uri: httpUrl })
]);

/**
 * defining the Websocket connection for Subscription
 * @accessToken: protcted path for Apollo Client
 * @lazy=true: means we'll start a websocket conn once we make our first graphql subscription
 * @lazy=false: means apollop client will start websocket connection as soon as the application is loaded
 * @connectionParams: we can use this option to pass extra values to the server when establishing graphql websocket connection and connectionParams can either be an object :{} or a function that returns an object (like below), using the function is usefull if the value inside the object can be changed overtime and you want to use the latest value available when the connection starts (i.e here the AccessToken changes when logged in/out ) that's why using as function here will call the getAccessToken func not as soon as the application loaded but rather when the connection starts
 */
const wsLink = new WebSocketLink({
  uri: wsUrl,
  options: {
    connectionParams: () => ({
      accessToken: getAccessToken()
    }),
    lazy: true,
    reconnect: true
  }
});

// Helper function
//---------------
function isSubscription(operation) {
  const definition = getMainDefinition(operation.query);
  return (
    definition.kind === 'OperationDefinition' &&
    definition.operation === 'subscription'
  );
}

// Split: using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent (query and mutation OR subscription)
//Now, queries and mutations will go over HTTP as normal, but subscriptions will be done over the websocket transport.
//---------------
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: split(isSubscription, wsLink, httpLink),
  defaultOptions: { query: { fetchPolicy: 'no-cache' } }
});

export default client;
