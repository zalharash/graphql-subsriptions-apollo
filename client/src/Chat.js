import React, { Component } from 'react';
import { addMessage, getMessages, onMessageAdded } from './graphql/queries';
import MessageInput from './MessageInput';
import MessageList from './MessageList';

class Chat extends Component {
  state = { messages: [] };
  subscription = null;

  //----------
  async componentDidMount() {
    const messages = await getMessages();
    this.setState({ messages });
    // Subscripe to message change
    this.subscription = onMessageAdded(message => {
      console.log('inside onMessageAdded');
      this.setState({ messages: this.state.messages.concat(message) });
    });
  }

  // this to stop the current componoent/client from receiveing more messages when user logged out, which result of react replacing/unmount the logged out comp with the logged in component
  //----------
  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscription();
    }
  }

  // curently the chat message is only visiable to the current user not to the others.
  //----------
  async handleSend(text) {
    await addMessage(text);
    // const message = await addMessage(text);
    // this.setState({ messages: this.state.messages.concat(message) });
  }
  //----------
  render() {
    const { user } = this.props;
    const { messages } = this.state;
    return (
      <section className='section'>
        <div className='container'>
          <h1 className='title'>Chatting as {user}</h1>
          <MessageList user={user} messages={messages} />
          <MessageInput onSend={this.handleSend.bind(this)} />
        </div>
      </section>
    );
  }
}

export default Chat;
