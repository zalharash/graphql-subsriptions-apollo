const fs = require('fs');
const http = require('http');
const { ApolloServer } = require('apollo-server-express');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const expressJwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const db = require('./db');
const resolvers = require('./resolvers');

const port = 9000;
const jwtSecret = Buffer.from('xkMBdsE+P6242Z2dPV3RD91BPbLIko7t', 'base64');
//-------------
const app = express();
app.use(
  cors(),
  bodyParser.json(),
  expressJwt({
    credentialsRequired: false,
    secret: jwtSecret
  })
);
const typeDefs = fs.readFileSync('./schema.graphql', { encoding: 'utf8' });

/**
 * Here we destruct two parameters directaly from the params and unlike HTTP request, here in Websocket
 * request we passed an additional param called connection that we will use to verify the "ws" connection
 * and extraxt the accesstoken.
 * @req.user: is set by expressJwt middleware by decoding the http header json object.
 * console log the params to see the whole object containing the connection and req and thier attributes
 */
//-------------
function context({ req, connection }) {
  if (req && req.user) {
    return { userId: req.user.sub };
  }
  //check if it's a Websocket connection
  if (connection && connection.context && connection.context.accessToken) {
    const decodedToken = jwt.verify(connection.context.accessToken, jwtSecret);
    return { userId: decodedToken.sub };
  }
  return {};
}
// Plug Apollo Server to express.
//-------------
const apolloServer = new ApolloServer({ typeDefs, resolvers, context });
apolloServer.applyMiddleware({ app, path: '/graphql' });

//-------------
app.post('/login', (req, res) => {
  const { name, password } = req.body;
  const user = db.users.get(name);
  if (!(user && user.password === password)) {
    res.sendStatus(401);
    return;
  }
  const token = jwt.sign({ sub: user.id }, jwtSecret);
  res.send({ token });
});

/**
 * -------------------------
 * Subscription - Enable Websocket:
 * To enable subscription, first we need to create our own http server and replace it with the one that
 * is created automatically for us by Express. we do that by init the httpserver and replace app.listen(..)
 * with our created server httpServer.listen(..) and then before we listen we tell Apollo server to instal
 * subscriptionHandlers on the newly created httpSever, that will enable the Websocket to be used for GraphQl
 */
const httpServer = http.createServer(app);
apolloServer.installSubscriptionHandlers(httpServer);
httpServer.listen(port, () => console.log(`Server started on port ${port}`));
