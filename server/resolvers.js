// PubSub stands for publish and subscripe
const { PubSub } = require('graphql-subscriptions');
const db = require('./db');

const pubSub = new PubSub();
const MESSAGE_ADDED = 'MESSAGE_ADDED';

function requireAuth(userId) {
  if (!userId) {
    throw new Error('Unauthorized');
  }
}

// Query:
// Here we get the userId from the context (the third param)
//------------------
const Query = {
  messages: (_root, _args, { userId }) => {
    requireAuth(userId);
    return db.messages.list();
  }
};
// Mutation:
//------------------
const Mutation = {
  addMessage: (_root, { input }, { userId }) => {
    requireAuth(userId);
    const messageId = db.messages.create({ from: userId, text: input.text });
    const message = db.messages.get(messageId);
    // Publish the message to the other subscribers
    pubSub.publish(MESSAGE_ADDED, { messageAdded: message });
    return message;
  }
};
// Subscritption:
// When a client subscribe to the MessageAdded Event, eachtime a new message published (by the mutation func) the pubSub instance will use the asyncIterator to notify all the subscribers with the new published msg
//------------------
const Subscription = {
  messageAdded: {
    // MESSAGE_ADDED: is the Event type
    subscribe: (_root, { input }, context) => {
      // console.log('messageAdded Context: ', context);
      requireAuth(context.userId);
      // Notify all the clients subscribers with the new msg
      return pubSub.asyncIterator(MESSAGE_ADDED);
    }
  }
};

module.exports = { Query, Mutation, Subscription };
