# GraphQL Chat Sample

Sample application used in the [GraphQL by Example](https://bit.ly/graphql-by-example) course.

## Project description:

The normal behavior of the chat message in the Chat.js component is to handle the message submit and incorporate the new message to the component's state which result in showing the new mesg in the web page, however, in order for other users to be able to see the msg they have to referesh their page. to resolve this issue we use GrpahQL Subscription.

### Subscription:

Subscription is another operation type just like Query and Mutation. It uses a different comunication protocol called ws (web socket, i.e ws://localhost:9000/graphql).
Just like how mutations describe the set of actions you can take to change your data, subscriptions define the set of events that you can subscribe to when data changes. In fact, you can think of subscriptions as a way to react to mutations that are performed else where.
Being able to subscribe to changing data has become increasingly important in modern applications. Why poll for data with expensive HTTP requests when we could just as easily make our app realtime? GraphQL subscriptions to the rescue! GraphQL subscriptions give us all the benefits of GraphQL in real-time.

#### WebSocket:

- Websocket uses ws protocol not http, it's important to distinguish bewteen them specially when adding Auth to http link, that's wont work with subscripe operation type since they're using "ws" protocol so we have to define the auth again for "ws" as well otherwise anyone can run Graphql playground and spies on us.
- The WebSocket API is an advanced technology that makes it possible to open a two-way interactive communication session between the user's browser and a server. With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.
  websockets are a way to instantly update your user interface. For example, Google Analytics has a real-time active users feature, if a new user joins your website then the count will increase and if a user leaves the count will decrease. With websockets, you can send out an update to anyone currently on your application, and change the UI, without a page refresh.

#### How the Subscription works:

Inside our resolvers.js in the backend, we add a Subscription resolver that returns an AsyncIterator, which listens to the events asynchronously. To generate events in the example, we notified the pubsub implementation inside of our Mutation resolver with publish. This publish call can occur outside of a resolver if required.

- Whenever you want a subscription to run in the backend, you publish some data to it from playground our the client side. In this case, my subscription is listening for bookTitleChanged. Whenever that gets published, any subscribers will receive that data.

### How we implemented the subscription in the Client?

1. client/graphql/client.js: Since the Subscription uses Websocket protocol to communicate with the server, we start by importing and defining websocket link ('ws://localhost ....), then we used split with ApolloClient to distiguish between links (httpLink or wsLink)Now, queries and mutations will go over HTTP as normal, but subscriptions will be done over the websocket transport.

2. client/graphql/queries.js: then we defined our subscripe function in this file along with the gql query that will be used later by the react components.

3. Chat.js: here in the componentDidMount() we would add the subscription func call right after we get the initial list of messages, so we get notified for any new message. so we added onMessageAdded() that will append the new message to the state so that it will be visiable to all the clients.

4. In the client.js: we implemented auth to protect the pass to post a new msg and restricted it only to the logged in users, using the connectionParams when initialized the WebSocketLink instance.

### How we implemented the subscription in the Server?

1. First we added the the Subscription type in schema.graphql file, Subscription is another operation type jsut like Query and mutation.

2. Then in the resolvers.js: We defined the subscribtion query and the subscribtion func that listen to MESSAGE_ADDED events asyncronosly. Eachtime a client (react client) publish a new message from the frontend, this func will be triggered to notify all the other clients subscribers that a new message is added.
   we received the context object as a third param then we extracted the userId to be able to verify if that's an authanticated user.
